package rabbitmq

import (
	"errors"
	"github.com/streadway/amqp"
	"gitlab.com/golight/gopubsub/queue"
	"testing"
	"time"
)

type MockConnect struct{}

func (c *MockConnect) Close() error {
	return nil
}

type MockChanel struct{}

func (m MockChanel) Ack(tag uint64, multiple bool) error {
	return nil
}

func (m MockChanel) Consume(queue, consumer string, autoAck, exclusive, noLocal, noWait bool, args amqp.Table) (<-chan amqp.Delivery, error) {
	if queue == "3" {
		return nil, errors.New("2")
	}
	return nil, nil
}

func (m MockChanel) QueueBind(name, key, exchange string, noWait bool, args amqp.Table) error {
	if exchange == "2" {
		return errors.New("2")
	}
	return nil
}

func (m MockChanel) QueueDeclare(name string, durable, autoDelete, exclusive, noWait bool, args amqp.Table) (amqp.Queue, error) {
	if name == "1" {
		return amqp.Queue{}, errors.New("1")
	}
	if name == "3" {
		return amqp.Queue{
			Name:      "3",
			Messages:  0,
			Consumers: 0,
		}, nil
	}
	return amqp.Queue{}, nil
}

func (m MockChanel) Publish(exchange, key string, mandatory, immediate bool, msg amqp.Publishing) error {
	return nil
}

func (m MockChanel) Close() error {
	return nil
}

type MockChanel2 struct{}

func (m MockChanel2) Ack(tag uint64, multiple bool) error {
	return nil
}

func (m MockChanel2) Consume(queue, consumer string, autoAck, exclusive, noLocal, noWait bool, args amqp.Table) (<-chan amqp.Delivery, error) {
	if queue == "3" {
		return nil, errors.New("2")
	}
	ch := make(chan amqp.Delivery)
	go func() {
		ch <- amqp.Delivery{
			Acknowledger:    nil,
			Headers:         nil,
			ContentType:     "",
			ContentEncoding: "",
			DeliveryMode:    0,
			Priority:        0,
			CorrelationId:   "",
			ReplyTo:         "",
			Expiration:      "",
			MessageId:       "",
			Timestamp:       time.Time{},
			Type:            "",
			UserId:          "",
			AppId:           "",
			ConsumerTag:     "",
			MessageCount:    0,
			DeliveryTag:     0,
			Redelivered:     false,
			Exchange:        "",
			RoutingKey:      "",
			Body:            nil,
		}
		close(ch)
	}()
	return ch, nil
}

func (m MockChanel2) QueueBind(name, key, exchange string, noWait bool, args amqp.Table) error {
	if exchange == "2" {
		return errors.New("2")
	}
	return nil
}

func (m MockChanel2) QueueDeclare(name string, durable, autoDelete, exclusive, noWait bool, args amqp.Table) (amqp.Queue, error) {
	if name == "1" {
		return amqp.Queue{}, errors.New("1")
	}
	if name == "3" {
		return amqp.Queue{
			Name:      "3",
			Messages:  0,
			Consumers: 0,
		}, nil
	}
	return amqp.Queue{}, nil
}

func (m MockChanel2) Publish(exchange, key string, mandatory, immediate bool, msg amqp.Publishing) error {
	return nil
}

func (m MockChanel2) Close() error {
	return errors.New("1")
}

func TestRabbitMQ_Ack(t *testing.T) {
	type fields struct {
		conn    Connectier
		channel Chaneler
	}
	type args struct {
		msg *queue.Message
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Ack",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel{},
			},
			args: args{msg: &queue.Message{
				Data:     nil,
				Err:      nil,
				Identity: uint64(0),
				Topic:    "",
			}},
			wantErr: false,
		},
		{
			name: "Ack",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel{},
			},
			args: args{msg: &queue.Message{
				Data:     nil,
				Err:      nil,
				Identity: nil,
				Topic:    "",
			}},
			wantErr: true,
		},
		{
			name: "Ack",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel2{},
			},
			args: args{msg: &queue.Message{
				Data:     nil,
				Err:      nil,
				Identity: nil,
				Topic:    "",
			}},
			wantErr: true,
		},
		{
			name: "Ack",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel2{},
			},
			args: args{msg: &queue.Message{
				Data:     nil,
				Err:      nil,
				Identity: uint64(0),
				Topic:    "",
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &RabbitMQ{
				conn:    tt.fields.conn,
				channel: tt.fields.channel,
			}
			if err := r.Ack(tt.args.msg); (err != nil) != tt.wantErr {
				t.Errorf("Ack() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestRabbitMQ_Close(t *testing.T) {
	type fields struct {
		conn    Connectier
		channel Chaneler
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "Close",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel{},
			},
			wantErr: false,
		},
		{
			name: "Close2",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel2{},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &RabbitMQ{
				conn:    tt.fields.conn,
				channel: tt.fields.channel,
			}
			if err := r.Close(); (err != nil) != tt.wantErr {
				t.Errorf("Close() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestRabbitMQ_Publish(t *testing.T) {
	type fields struct {
		conn    Connectier
		channel Chaneler
	}
	type args struct {
		topic   string
		message []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Publish",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel{},
			},
			args: args{
				topic:   "",
				message: nil,
			},
			wantErr: false,
		},
		{
			name: "Publish",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel2{},
			},
			args: args{
				topic:   "",
				message: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &RabbitMQ{
				conn:    tt.fields.conn,
				channel: tt.fields.channel,
			}
			if err := r.Publish(tt.args.topic, tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("Publish() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestRabbitMQ_Subscribe(t *testing.T) {
	type fields struct {
		conn    Connectier
		channel Chaneler
	}
	type args struct {
		topic string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    <-chan queue.Message
		wantErr bool
	}{
		{
			name: "Subscribe",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel{},
			},
			args:    args{topic: ""},
			want:    make(<-chan queue.Message),
			wantErr: false,
		},
		{
			name: "Subscribe",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel2{},
			},
			args:    args{topic: ""},
			want:    make(<-chan queue.Message),
			wantErr: false,
		},
		{
			name: "Subscribe",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel{},
			},
			args:    args{topic: "1"},
			want:    make(<-chan queue.Message),
			wantErr: true,
		},
		{
			name: "Subscribe",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel2{},
			},
			args:    args{topic: "1"},
			want:    make(<-chan queue.Message),
			wantErr: true,
		},
		{
			name: "Subscribe",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel{},
			},
			args:    args{topic: "2"},
			want:    make(<-chan queue.Message),
			wantErr: true,
		},
		{
			name: "Subscribe",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel2{},
			},
			args:    args{topic: "2"},
			want:    make(<-chan queue.Message),
			wantErr: true,
		},
		{
			name: "Subscribe",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel{},
			},
			args:    args{topic: "3"},
			want:    make(<-chan queue.Message),
			wantErr: true,
		},
		{
			name: "Subscribe",
			fields: fields{
				conn:    &MockConnect{},
				channel: &MockChanel2{},
			},
			args:    args{topic: "3"},
			want:    make(<-chan queue.Message),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &RabbitMQ{
				conn:    tt.fields.conn,
				channel: tt.fields.channel,
			}
			_, err := r.Subscribe(tt.args.topic)
			if (err != nil) != tt.wantErr {
				t.Errorf("Subscribe() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
