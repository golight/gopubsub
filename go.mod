module gitlab.com/golight/gopubsub

go 1.19

require (
	github.com/confluentinc/confluent-kafka-go v1.9.2
	github.com/nats-io/nats.go v1.17.0
	github.com/streadway/amqp v1.1.0
)

require (
	github.com/klauspost/compress v1.17.6 // indirect
	github.com/nats-io/nkeys v0.4.7 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	golang.org/x/crypto v0.19.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
)
