# Gopubsub
Пакет представляет собой реализацию очереди сообщений с использованием Nuts, Rabbit, Nats

## Запуск проекта

```bash
docker-compose up -d
```

## Запуск приложений

Pubsub RabbitMQ

Dashboard `http://localhost:15672/`

```bash
go run cmd/rpubsub/main.go
```


Pubsub Kafka

```bash
go run cmd/kpubsub/main.go
```

Pubsub Nats

```bash
go run cmd/natsmq/main.go
```
## Методы

```go
// Message структура представляет сообщение в очереди.
// Она содержит данные сообщения, возможную ошибку, идентификатор для доставки (в RabbitMQ это тег доставки, в Kafka - смещение) и тему, к которой принадлежит сообщение.
type Message struct {
	Data     []byte // Данные сообщения в виде массива байтов.
	Err      error  // Ошибка, которая может возникнуть при обработке сообщения.
	Identity any    // Идентификатор для доставки, который может быть использован как тег доставки в RabbitMQ или смещение в Kafka.
	Topic    string // Тема, к которой принадлежит сообщение.
}

// MessageQueuer интерфейс определяет методы для работы с очередью сообщений.
type MessageQueuer interface {
	Publish(topic string, message []byte) error // Публикация сообщения в тему.
	Subscribe(topic string) (<-chan Message, error) // Подписка на тему и получение канала для чтения сообщений.
	Ack(msg *Message) error // Подтверждение получения сообщения.
	Close() error // Закрытие соединения с очередью сообщений.
}

```
## Пример реализации Kafka
```go
package main

import (
	"fmt"
	"gitlab.com/ptflp/gopubsub/kafkamq"
	"log"
	"time"
)

func main() {
	// Создание нового экземпляра очереди сообщений с Kafka.
	mq, err := kafkamq.NewKafkaMQ("localhost:9092", "myGroup")
	if err != nil {
		log.Fatalf("Failed to create MQ: %s\n", err)
	}

	// Тема, на которую будет подписываться приложение.
	topic := "myTopic"

	// Подписка на тему.
	msgs, err := mq.Subscribe(topic)
	if err != nil {
		log.Fatalf("Failed to subscribe to topic: %s\n", err)
	}

	// Счетчик для формирования сообщений.
	n :=  1
	for {
		// Обработка входящих сообщений и публикация новых сообщений.
		select {
		case msg, ok := <-msgs:
			if !ok {
				// Если канал закрыт, выход из цикла.
				return
			}
			if msg.Err != nil {
				// Обработка ошибок при получении сообщения.
				log.Fatalf("Failed to receive message: %s\n", msg.Err)
			}
			// Вывод полученного сообщения.
			fmt.Printf("Received message: %s\n", string(msg.Data))
			// Подтверждение получения сообщения.
			err = mq.Ack(&msg)
			if err != nil {
				// Обработка ошибок при подтверждении получения сообщения.
				log.Fatalf("Failed to ack message: %s\n", err)
			}
		default:
			// Если нет входящих сообщений, ожидание и публикация нового сообщения.
			time.Sleep(100 * time.Millisecond)
			err = mq.Publish(topic, []byte(fmt.Sprintf("Message kafka %d", n)))
			if err != nil {
				// Обработка ошибок при публикации сообщения.
				log.Fatalf("Failed to publish message: %s\n", err)
			}
			n++
		}
	}
}


```

## Пример реализации Rabbit

```go
package main

import (
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/ptflp/gopubsub/rabbitmq"
	"log"
	"time"
)

func main() {
	// Создание соединения с RabbitMQ.
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	// Создание нового экземпляра RabbitMQ с использованием соединения.
	rmq, err := rabbitmq.NewRabbitMQ(conn)
	if err != nil {
		panic(err)
	}
	defer rmq.Close()

	// Создание обменника (exchange) с типом "direct".
	if err := rabbitmq.CreateExchange(conn, "test", "direct"); err != nil {
		log.Fatal(err)
	}

	// Подписка на обменник с именем "test".
	messages, err := rmq.Subscribe("test")
	if err != nil {
		panic(err)
	}

	// Создание таймера с интервалом в  10 минут.
	ticker := time.NewTicker(600 * time.Second)
	defer ticker.Stop()

	// Счетчик для формирования сообщений.
	n :=  1
	for {
		select {
		case <-ticker.C:
			// При наступлении интервала таймера, цикл продолжается, но не делает ничего.
			break
		case msg := <-messages:
			// Обработка входящих сообщений и подтверждение их получения.
			err := rmq.Ack(&msg)
			if err != nil {
				panic(err)
			}
			fmt.Println(string(msg.Data))
		default:
			// Если нет входящих сообщений, ожидание и публикация нового сообщения каждые  100 мс.
			time.Sleep(100 * time.Millisecond)
			err = rmq.Publish("test", []byte(fmt.Sprintf("message Rabbitmq %d", n)))
			if err != nil {
				panic(err)
			}
			n++
		}
	}
	rmq.Close()
}


```
## Пример реализации Nats

```go
package main

import (
	"fmt"
	"gitlab.com/golight/gopubsub/nutsmq"
	"log"
	"strconv"
	"time"
)

func main() {
	mq, err := nutsmq.NewNatsMQ("npubsub://localhost:4222")
	topic := "myTopic"
	msgs, err := mq.Subscribe(topic)
	if err != nil {
		log.Fatalf("Не удалось подписатьсся на topic: %s\n", err)
	}

	n := 0
	for {
		select {
		case msg, ok := <-msgs:
			if !ok {
				return
			}
			if msg.Err != nil {
				fmt.Println("Не удалось получить сообщение:", msg.Err)
				return
			}
			fmt.Printf("Получено сообщение: %s\n", string(msg.Data))
		default:
			time.Sleep(1000 * time.Millisecond)
			err = mq.Publish(topic, []byte(strconv.Itoa(n)))
			if err != nil {
				fmt.Println("Не удалось опубликовать сообщение:", err)
				return
			}
			n++
		}
	}
}

```