package main

import (
	"fmt"
	"gitlab.com/golight/gopubsub/natsmq"
	"strconv"
	"time"
)

func main() {
	mq, err := natsmq.NewNatsMQ("npubsub://localhost:4222")
	if err != nil {
		fmt.Println("Не удалось подключиться к серверу NATS:", err)
		return
	}
	topic := "myTopic"
	msgs, err := mq.Subscribe(topic)
	if err != nil {
		fmt.Printf("Не удалось подписатьсся на topic: %s\n", err)
	}

	n := 0
	for {
		select {
		case msg, ok := <-msgs:
			if !ok {
				return
			}
			if msg.Err != nil {
				fmt.Println("Не удалось получить сообщение:", msg.Err)
				return
			}
			fmt.Printf("Получено сообщение: %s\n", string(msg.Data))
		default:
			time.Sleep(1000 * time.Millisecond)
			err = mq.Publish(topic, []byte(strconv.Itoa(n)))
			if err != nil {
				fmt.Println("Не удалось опубликовать сообщение:", err)
				return
			}
			n++
		}
	}
}
