package natsmq

import (
	"github.com/nats-io/nats.go"
	"gitlab.com/golight/gopubsub/queue"
)

type NatsMQ struct {
	NC Consumerer
}

type Consumerer interface {
	Publish(subj string, data []byte) error
	Subscribe(subj string, cb nats.MsgHandler) (*nats.Subscription, error)
	Close()
}

func NewNatsMQ(broker string) (queue.MessageQueuer, error) {
	nc, err := nats.Connect(broker)
	if err != nil {
		return nil, err
	}
	return &NatsMQ{NC: nc}, err
}
func (n *NatsMQ) Publish(topic string, message []byte) error {
	return n.NC.Publish(topic, message)
}

func (n *NatsMQ) Subscribe(topic string) (<-chan queue.Message, error) {
	ch := make(chan queue.Message)
	_, err := n.NC.Subscribe(topic, func(m *nats.Msg) {
		ch <- queue.Message{
			Data:  m.Data,
			Topic: m.Subject,
		}
	})
	return ch, err
}

func (n *NatsMQ) Ack(msg *queue.Message) error {
	// NATS не требует подтверждения сообщений, поэтому этот метод может быть пустым.
	return nil
}

func (n *NatsMQ) Close() error {
	n.NC.Close()
	return nil
}
