package natsmq

import (
	"github.com/nats-io/nats.go"
	"gitlab.com/golight/gopubsub/queue"
	"reflect"
	"testing"
)

type MockNatsConsumer struct{}

func (m *MockNatsConsumer) Publish(subj string, data []byte) error {
	return nil
}
func (m *MockNatsConsumer) Subscribe(subj string, cb nats.MsgHandler) (*nats.Subscription, error) {
	return &nats.Subscription{}, nil
}
func (m *MockNatsConsumer) Close() {}
func TestNatsMQ_Ack(t *testing.T) {
	type fields struct {
		NC Consumerer
	}
	type args struct {
		msg *queue.Message
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "Ack",
			fields:  fields{NC: &MockNatsConsumer{}},
			args:    args{msg: nil},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &NatsMQ{
				NC: tt.fields.NC,
			}
			if err := n.Ack(tt.args.msg); (err != nil) != tt.wantErr {
				t.Errorf("Ack() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNatsMQ_Close(t *testing.T) {
	type fields struct {
		NC Consumerer
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name:    "Close",
			fields:  fields{NC: &MockNatsConsumer{}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &NatsMQ{
				NC: tt.fields.NC,
			}
			if err := n.Close(); (err != nil) != tt.wantErr {
				t.Errorf("Close() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNatsMQ_Publish(t *testing.T) {
	type fields struct {
		NC Consumerer
	}
	type args struct {
		topic   string
		message []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "Publish",
			fields:  fields{NC: &MockNatsConsumer{}},
			args:    args{topic: "", message: make([]byte, 0)},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &NatsMQ{
				NC: tt.fields.NC,
			}
			if err := n.Publish(tt.args.topic, tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("Publish() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNatsMQ_Subscribe(t *testing.T) {
	type fields struct {
		NC Consumerer
	}
	type args struct {
		topic string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    <-chan queue.Message
		wantErr bool
	}{
		{
			name:    "Subscribe",
			fields:  fields{NC: &MockNatsConsumer{}},
			args:    args{topic: ""},
			want:    make(<-chan queue.Message),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &NatsMQ{
				NC: tt.fields.NC,
			}
			got, err := n.Subscribe(tt.args.topic)
			if (err != nil) != tt.wantErr {
				t.Errorf("Subscribe() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if reflect.TypeOf(tt.want) != reflect.TypeOf(got) {
				t.Errorf("Subscribe() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewNatsMQ(t *testing.T) {
	type args struct {
		broker string
	}
	tests := []struct {
		name    string
		args    args
		want    queue.MessageQueuer
		wantErr bool
	}{
		{
			name:    "NewNatsMQ",
			args:    args{broker: ""},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewNatsMQ(tt.args.broker)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewNatsMQ() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if reflect.TypeOf(got) != reflect.TypeOf(tt.want) {
				t.Errorf("NewNatsMQ() got = %v, want %v", got, tt.want)
			}
		})
	}
}
