package kafkamq

import (
	"errors"
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"gitlab.com/golight/gopubsub/queue"
	"reflect"
	"testing"
)

type MockKafkaConsumerer struct {
}

type MockKafkaProducerer struct {
}

func (m *MockKafkaConsumerer) SubscribeTopics(topics []string, rebalanceCb kafka.RebalanceCb) (err error) {
	if topics[0] == "1" {
		return errors.New("1")
	}
	return nil
}

func (m *MockKafkaConsumerer) CommitOffsets(offsets []kafka.TopicPartition) ([]kafka.TopicPartition, error) {
	return []kafka.TopicPartition{}, nil
}

func (m *MockKafkaConsumerer) Close() (err error) {
	return nil
}

func (m *MockKafkaProducerer) Produce(msg *kafka.Message, deliveryChan chan kafka.Event) error {
	return nil
}
func (m *MockKafkaProducerer) Close() {
}

func TestMQ_Ack(t *testing.T) {
	type fields struct {
		Producer  Producerer
		Consumer  Consumerer
		topicsOut map[string]chan queue.Message
		topics    []string
	}
	type args struct {
		msg *queue.Message
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "ask",
			fields: fields{
				Producer:  &MockKafkaProducerer{},
				Consumer:  &MockKafkaConsumerer{},
				topicsOut: make(map[string]chan queue.Message),
				topics:    make([]string, 0),
			},
			args: args{msg: &queue.Message{
				Data:     []byte{},
				Err:      nil,
				Identity: kafka.TopicPartition{},
				Topic:    "",
			}},
			wantErr: false,
		},
		{
			name: "ask2",
			fields: fields{
				Producer:  &MockKafkaProducerer{},
				Consumer:  &MockKafkaConsumerer{},
				topicsOut: make(map[string]chan queue.Message),
				topics:    make([]string, 0),
			},
			args: args{msg: &queue.Message{
				Data:     []byte{},
				Err:      nil,
				Identity: nil,
				Topic:    "",
			}},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := &MQ{
				Producer:  tt.fields.Producer,
				Consumer:  tt.fields.Consumer,
				topicsOut: tt.fields.topicsOut,
				topics:    tt.fields.topics,
			}
			if err := k.Ack(tt.args.msg); (err != nil) != tt.wantErr {
				t.Errorf("Ack() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMQ_Close(t *testing.T) {
	type fields struct {
		Producer  Producerer
		Consumer  Consumerer
		topicsOut map[string]chan queue.Message
		topics    []string
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "close",
			fields: fields{
				Producer:  &MockKafkaProducerer{},
				Consumer:  &MockKafkaConsumerer{},
				topicsOut: make(map[string]chan queue.Message),
				topics:    make([]string, 0),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := &MQ{
				Producer:  tt.fields.Producer,
				Consumer:  tt.fields.Consumer,
				topicsOut: tt.fields.topicsOut,
				topics:    tt.fields.topics,
			}
			if err := k.Close(); (err != nil) != tt.wantErr {
				t.Errorf("Close() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMQ_Publish(t *testing.T) {
	type fields struct {
		Producer  Producerer
		Consumer  Consumerer
		topicsOut map[string]chan queue.Message
		topics    []string
	}
	type args struct {
		topic   string
		message []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Publish",
			fields: fields{
				Producer:  &MockKafkaProducerer{},
				Consumer:  &MockKafkaConsumerer{},
				topicsOut: make(map[string]chan queue.Message),
				topics:    make([]string, 0),
			},
			args: args{
				topic:   "",
				message: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := &MQ{
				Producer:  tt.fields.Producer,
				Consumer:  tt.fields.Consumer,
				topicsOut: tt.fields.topicsOut,
				topics:    tt.fields.topics,
			}
			if err := k.Publish(tt.args.topic, tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("Publish() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMQ_Subscribe(t *testing.T) {
	type fields struct {
		Producer  Producerer
		Consumer  Consumerer
		topicsOut map[string]chan queue.Message
		topics    []string
	}
	type args struct {
		topic string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    <-chan queue.Message
		wantErr bool
	}{
		{
			name: "Subscribe",
			fields: fields{
				Producer:  &MockKafkaProducerer{},
				Consumer:  &MockKafkaConsumerer{},
				topicsOut: make(map[string]chan queue.Message),
				topics:    make([]string, 0),
			},
			args: args{
				topic: "",
			},
			wantErr: false,
		},
		{
			name: "Subscribe2",
			fields: fields{
				Producer:  &MockKafkaProducerer{},
				Consumer:  &MockKafkaConsumerer{},
				topicsOut: make(map[string]chan queue.Message),
				topics:    make([]string, 0),
			},
			args: args{
				topic: "1",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := &MQ{
				Producer:  tt.fields.Producer,
				Consumer:  tt.fields.Consumer,
				topicsOut: tt.fields.topicsOut,
				topics:    tt.fields.topics,
			}
			_, err := k.Subscribe(tt.args.topic)
			if (err != nil) != tt.wantErr {
				t.Errorf("Subscribe() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestNewKafkaMQ(t *testing.T) {
	p, _ := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": ""})
	c, _ := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":    "",
		"auto.offset.reset":    "earliest",
		"group.id":             "",
		"max.poll.interval.ms": 12000,
		"session.timeout.ms":   12000,
	})
	topicsOut := make(map[string]chan queue.Message)
	type args struct {
		broker  string
		groupID string
	}
	tests := []struct {
		name    string
		args    args
		want    queue.MessageQueuer
		wantErr bool
	}{
		{
			name: "NewKafkaMQ",
			args: args{
				broker:  "",
				groupID: "",
			},
			want: &MQ{
				Producer:  p,
				Consumer:  c,
				topicsOut: topicsOut,
				topics:    nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewKafkaMQ(tt.args.broker, tt.args.groupID)
			fmt.Println(err)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewKafkaMQ() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if reflect.TypeOf(got) != reflect.TypeOf(tt.want) {
				t.Errorf("NewKafkaMQ() got = %v, want %v", got, tt.want)
			}
		})
	}
}
