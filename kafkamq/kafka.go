package kafkamq

import (
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"gitlab.com/golight/gopubsub/queue"
	"log"
)

// MQ структура представляет собой очередь сообщений с использованием Kafka.
// Она содержит продюсера и консьюмера Kafka, а также каналы для каждого темы.
type MQ struct {
	Producer  Producerer
	Consumer  Consumerer
	topicsOut map[string]chan queue.Message
	topics    []string
}

// PartitionOffset структура хранит информацию о разделе и смещении в Kafka.
type PartitionOffset struct {
	Partition int32
	Offset    kafka.Offset
	Topic     *string
}
type Consumerer interface {
	SubscribeTopics(topics []string, rebalanceCb kafka.RebalanceCb) (err error)
	CommitOffsets(offsets []kafka.TopicPartition) ([]kafka.TopicPartition, error)
	Close() (err error)
}
type Producerer interface {
	Produce(msg *kafka.Message, deliveryChan chan kafka.Event) error
	Close()
}

// NewKafkaMQ функция создает новую очередь сообщений с Kafka.
// Она принимает адрес брокера и ID группы в качестве аргументов.
func NewKafkaMQ(broker, groupID string) (queue.MessageQueuer, error) {
	// Создание продюсера Kafka.
	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": broker})
	if err != nil {
		return nil, err
	}

	// Создание консьюмера Kafka.
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":    broker,
		"auto.offset.reset":    "earliest",
		"group.id":             groupID,
		"max.poll.interval.ms": 12000,
		"session.timeout.ms":   12000,
	})
	if err != nil {
		return nil, err
	}

	// Инициализация каналов для каждого темы.
	topicsOut := make(map[string]chan queue.Message)

	// Запуск горутины для чтения сообщений из Kafka.
	go func() {
		for {
			var msg *kafka.Message
			msg, err = c.ReadMessage(-1)
			if err != nil {
				// Обработка ошибки чтения сообщения.
				for _, ch := range topicsOut {
					ch <- queue.Message{Err: err}
				}
				log.Println(err)
				continue
			}
			// Отправка прочитанного сообщения в соответствующий канал.
			topicsOut[*msg.TopicPartition.Topic] <- queue.Message{
				Topic: *msg.TopicPartition.Topic,
				Data:  msg.Value,
				// В Kafka offset может быть использован вместо DeliveryTag в RabbitMQ.
				Identity: msg.TopicPartition,
			}
		}
	}()

	// Возвращение нового экземпляра MQ с настроенными продюсером и консьюмером.
	return &MQ{
		Producer:  p,
		Consumer:  c,
		topicsOut: topicsOut,
	}, nil
}

// Publish метод публикует сообщение в указанную тему.
func (k *MQ) Publish(topic string, message []byte) error {
	msg := &kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Value:          message,
	}
	return k.Producer.Produce(msg, nil)
}

// Subscribe метод подписывается на указанную тему и возвращает канал для чтения сообщений.
func (k *MQ) Subscribe(topic string) (<-chan queue.Message, error) {
	k.topics = append(k.topics, topic)
	err := k.Consumer.SubscribeTopics(k.topics, nil)
	if err != nil {
		return nil, err
	}
	out := make(chan queue.Message)
	k.topicsOut[topic] = out

	return out, nil
}

// Ack метод подтверждает получение сообщения.
func (k *MQ) Ack(msg *queue.Message) error {
	var err error
	if v, ok := msg.Identity.(kafka.TopicPartition); ok {
		_, err = k.Consumer.CommitOffsets([]kafka.TopicPartition{v})
		return err
	}

	return fmt.Errorf("invalid identity type %T", msg.Identity)
}

// Close метод закрывает продюсера и консьюмера Kafka.
func (k *MQ) Close() error {
	k.Producer.Close()
	return k.Consumer.Close()
}
